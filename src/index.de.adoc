= Walkaway Manifest
:sectlinks:
:toc:

Eine neue UN-Resolution muss her.

== Überzeugung

Wir glauben, dass Waffengewalt auch zur Verteidigung verheerend ist.

Gewalt führt zu Leid, Hass und Tod. Kriegserlebnisse und physischer wie psychischer Terror, Verluste und Leid führen zu Traumata. Wir wünschen niemandem solche Erlebnisse. Die Folgen sind Armut, Verlust, Entwurzelung und Hass und eine Prägung durch den Krieg und die Gewalt. Als Folge erleben wir auch, dass Opfer zu Tätern werden und die Spirale sich weiter dreht.

== Absicht

Wir wollen in einer Welt leben, in der diese Werte feste Ziele sind:

- Respektvoller und würdevoller Umgang mit Mensch und Tier
- Klimaschutz und nachhaltiger Umgang mit Ressourcen
- Sozialer Frieden
- Gerechtigkeit
- Glück

Aktuell müssen wir feststellen: Unser Selbstbild einer zivilisierten Menschheit steht in starkem Kontrast zu unserem barbarischen Umgang mit den oben genannten Werten.

Wir glauben, die Menschheit ist zivilisiert genug, sich anständig zu benehmen.

== Der Weg dahin

Wir glauben, dass die UN eine Resolution erlassen sollte, die folgende Dinge klarstellt:

- Es wird kein Krieg mehr geführt
- Wenn Krieg geführt wird, verlassen alle Menschen geordnet das angegriffene Land, damit kein schlimmer Schaden entsteht
- Alle anderen Länder verpflichten sich, die Geflüchteten würdig und auf unbestimmte Zeit aufzunehmen
- Die Invasion findet dann ohne Kampfhandlungen statt; bevor das Land zerbombt wird, überlassen wir es lieber dem Aggressor
- Als Konsequenz darf **mit dem Aggressor lebenslang nie wieder ein Geschäft gemacht werden**
- Das gilt mindestens für alle hochrangigen Entscheidungsträger des Aggressors und ist eine **persönliche** Sache
- Keine Bank nimmt sein Geld, niemand darf ihm Geld leihen oder ihn anstellen und sein Vermögen wird eingefroren
- Das Land des Aggressors muss mit mindestens 10 Jahren spürbaren Sanktionen rechnen:
Lasst es nicht dazu kommen und wehrt Euch gegen die Machthaber, wenn es doch so weit kommt

== Abwägungen

Es ist uns klar, dass dieses Zurückweichen ebenfalls zu Entwurzelungen und Leid führen würde.  +
Die Heimat ginge verloren, Freunde und womöglich die Familie würden getrennt. Geschäfte, die vielleicht über Generationen aufgebaut wurden, fielen dem Aggressor in die Hände und die Betreiber wären gezwungen, alles anderswo neu aufzubauen.

Das Leid wäre aber geringer, schon weil durch Krieg niemand sterben müsste und es zu keinen Misshandlungen und keiner Folter als Kriegsfolge käme. Es gäbe neue Optionen für interkulturelles Verständnis und Freundschaften.

Die durch einen Krieg entstehenden primären und sekundären Kosten könnten gänzlich in soziale Projekte investiert werden.

Wir glauben, dass bei einer konsequenten Durchsetzung der pflichtbewussten UN-Mitgliedsstaaten in Zukunft fast keine Kriege mehr geführt würden, weil wesentliche Gründe für Aggressoren entfallen:

- Kapital und Macht lassen sich durch Annexion oder Statthalter nicht weiter mehren, weil die Aggressoren **persönlich isoliert und enteignet** werden
- Weniger Krieg bedeutet zukünftig weniger Hass auf ehemalige Aggressoren

Wir glauben, die **persönliche Isolation und Enteignung** des Aggressors und seiner hochrangigen Entscheidungsträger, ist wirkungsvoller als die Androhung einer strafbewehrten Verurteilung vor dem Kriegsgerichtshof in Den Haag, der man als Mächtiger ohnehin allzu leicht entgeht.

Wir möchten Szenarien diskutieren und unsere Hypothese durch Zahlen belegen. So soll diese junge Idee reifen und sich zu einer ernsthaften Option für die Zukunft entwickeln.
